import os
import matplotlib.pyplot as plt
sizes = []
times = []
pc_type = ('ilu', 'gamg')
color = ('r', 'b')
for j in range(2):
	print(pc_type[j])
	for k in range(5):
		Nx 	= 10*2**k # ** is the exponentiation
		modname = 'perf%d' % k
		options = ['-snes_type ngmres', '-pc_type '+pc_type[j], '-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' %modname]
		print(options)
		os.system('./bin/ex5 '+ ' '.join(options)) # Calls and runs ex5
		perfmod = __import__(modname)
		sizes.append(Nx**2)
		times.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
		print zip(sizes, times)
		plt.plot(sizes, times)
plt.title('SNES ex5')
plt.xlabel('Problem Size $N$')
plt.ylabel('Time(s)')
plt.show()

 
	
